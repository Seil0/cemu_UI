/**
 * cemu_UI
 * 
 * Copyright 2017-2019  <@Seil0>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

package com.cemu_UI.application;
	
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cemu_UI.controller.CloudController;
import com.cemu_UI.controller.XMLController;
import com.cemu_UI.uiElements.JFXInfoAlert;
import com.cemu_UI.uiElements.JFXOkayCancelAlert;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

public class Main extends Application {
	
	private final String gamesDBdownloadURL = "https://git.mosad.xyz/Seil0/cemu_UI/raw/branch/master/downloadContent/games.db";
	private static Main main;
	private static XMLController xmlController;
	private MainWindowController mainWindowController;
	private CloudController cloudController;
	private Stage primaryStage;
	private AnchorPane pane;
	private Scene scene;

    private static Logger LOGGER;
	
	@Override
	public void start(Stage primaryStage) {
		try {
			LOGGER.info("OS: " + XMLController.getOsName() + " " + XMLController.getOsVers() + " " + XMLController.getOsVers());
			LOGGER.info("Java: " + XMLController.getJavaVend() + " " + XMLController.getJavaVers());
			LOGGER.info("User: " + XMLController.getUserName() + " " + XMLController.getUserHome());
			
			this.primaryStage = primaryStage;
			mainWindowController = new MainWindowController();
			main = this;
			
			mainWindow();
			initActions();
		} catch (Exception e) {
			LOGGER.error("ooooops",e);
		}
		
	}
	
	private void mainWindow(){
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/fxml/MainWindow.fxml"));
			loader.setController(mainWindowController);
			pane = (AnchorPane) loader.load();
			primaryStage.setMinWidth(1130);
			primaryStage.setMinHeight(600 + 34);
			primaryStage.setTitle("cemu_UI");
//			primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream(""))); //adds application icon
			primaryStage.setOnCloseRequest(event -> System.exit(0));
			
			// generate window
			scene = new Scene(pane); // create new scene, append pane to scene
			scene.getStylesheets().add(Main.class.getResource("/css/MainWindows.css").toExternalForm());
			primaryStage.setScene(scene); // append scene to stage
			primaryStage.show(); // show stage
			
			cloudController = CloudController.getInstance(mainWindowController); // call cloudController constructor
			
			// startup checks
			// check if client_secret.json is present
			if (Main.class.getResourceAsStream("/client_secret.json") == null) {
				LOGGER.error("client_secret is missing!!!!!");
				
				JFXInfoAlert noCSAlert = new JFXInfoAlert("Error",
						"client_secret is missing! Please contact the maintainer. \n"
						+ "If you compiled cemu_UI by yourself see: \n"
						+ "https://git.mosad.xyz/Seil0/cemu_UI/wiki/Documantation",
						"-fx-button-type: RAISED; -fx-background-color: #00a8cc; -fx-text-fill: BLACK;", primaryStage);
				noCSAlert.showAndWait();
			}

			if (!XMLController.getDirCemuUI().exists()) {
				LOGGER.info("creating cemu_UI directory");
				XMLController.getDirCemuUI().mkdir();
				XMLController.getPictureCache().mkdir();
			}
			
			if (!XMLController.getConfigFile().exists()) {
				LOGGER.info("firststart, setting default values");
				firstStart();
				xmlController.saveSettings();
			}
			
			if (!XMLController.getPictureCache().exists()) {
				XMLController.getPictureCache().mkdir();
			}
			
			if (!XMLController.getRference_gamesFile().exists()) {
				if (XMLController.getGamesDBFile().exists()) {
					XMLController.getGamesDBFile().delete();
				}
				try {
					LOGGER.info("downloading ReferenceGameList.db... ");
					URL website = new URL(gamesDBdownloadURL);
					ReadableByteChannel rbc = Channels.newChannel(website.openStream());
					FileOutputStream fos = new FileOutputStream(XMLController.getRference_gamesFile());
					fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
					fos.close();
					LOGGER.info("finished downloading games.db");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			// init here as it loads the games to the mwc and the gui, therefore the window must exist
			mainWindowController.init();
			
			// if cloud sync is activated start sync
			if (XMLController.isCloudSync()) {
				cloudController.initializeConnection(XMLController.getCloudService(), XMLController.getCemuPath());
				cloudController.sync(XMLController.getCloudService(), XMLController.getCemuPath(), XMLController.getDirCemuUIPath());
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		String logPath = "";
		
		if (System.getProperty("os.name").contains("Windows")) {
			logPath = System.getProperty("user.home") + "/Documents/cemu_UI/app.log";
		} else {
			logPath = System.getProperty("user.home") + "/cemu_UI/app.log";
		}
		
		System.setProperty("logFilename", logPath);
		File logFile = new File(logPath);
		logFile.delete();
		LOGGER = LogManager.getLogger(Main.class.getName());
		
		xmlController = new XMLController();
		launch(args);
	}
	
	private void firstStart() {
	
		JFXOkayCancelAlert cemuInstallAlert = new JFXOkayCancelAlert("cemu installation",
				"please select your cemu installation",
				"-fx-button-type: RAISED; -fx-background-color: #00a8cc; -fx-text-fill: BLACK;", primaryStage);
		cemuInstallAlert.setOkayAction(e -> {
			DirectoryChooser directoryChooser = new DirectoryChooser();
            File selectedDirectory = directoryChooser.showDialog(primaryStage);
            XMLController.setCemuPath(selectedDirectory.getAbsolutePath());
		});
		cemuInstallAlert.setCancelAction(e -> {
			XMLController.setCemuPath(null);
			LOGGER.info("Action canceld by user!");
		});
		cemuInstallAlert.showAndWait();
		
		JFXOkayCancelAlert romDirectoryAlert = new JFXOkayCancelAlert("rom directory",
				"please select your rom directory",
				"-fx-button-type: RAISED; -fx-background-color: #00a8cc; -fx-text-fill: BLACK;", primaryStage);
		romDirectoryAlert.setOkayAction(e -> {
			DirectoryChooser directoryChooser = new DirectoryChooser();
			File selectedDirectory = directoryChooser.showDialog(primaryStage);
			XMLController.setRomDirectoryPath(selectedDirectory.getAbsolutePath());
		});
		romDirectoryAlert.setCancelAction(e -> {
			XMLController.setRomDirectoryPath(null);
			LOGGER.info("Action canceld by user!");
		});
		romDirectoryAlert.showAndWait();
		
	}
	
	private void initActions() {
		final ChangeListener<Number> widthListener = new ChangeListener<Number>() {

			final Timer timer = new Timer();
			TimerTask saveTask = null; // task to execute save operation
			final long delayTime = 500; // delay until the window size is saved, if the window is resized earlier it will be killed, default is 500ms
			
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, final Number newValue) {
				
				// if saveTask is already running kill it
				if (saveTask != null) saveTask.cancel();

				saveTask = new TimerTask() {
					@Override
				    public void run() {
						XMLController.setWindowWidth(mainWindowController.getMainAnchorPane().getWidth());
						xmlController.saveSettings();
					}
				};
				timer.schedule(saveTask, delayTime);
			}
		};
		
		final ChangeListener<Number> heightListener = new ChangeListener<Number>() {
			
			final Timer timer = new Timer();
			TimerTask saveTask = null; // task to execute save operation
			final long delayTime = 500; // delay until the window size is saved, if the window is resized earlier it will be killed, default is 500ms
			
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, final Number newValue) {
				
				if (saveTask != null) saveTask.cancel();
				
				saveTask = new TimerTask() {
					@Override
				    public void run() {
						XMLController.setWindowHeight(mainWindowController.getMainAnchorPane().getHeight());
						xmlController.saveSettings();
					}
				};
				timer.schedule(saveTask, delayTime);
			}
		};
		
		final ChangeListener<Boolean> maximizeListener = new ChangeListener<Boolean>() {
			
		    @Override
		    public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
            	primaryStage.setMaximized(false);
		    	
		    	Alert alert = new Alert(AlertType.WARNING);
            	alert.setTitle("edit");
            	alert.setHeaderText("cemu_UI");
            	alert.setContentText("maximized Window is not supporte!");
            	alert.initOwner(primaryStage);
            	alert.showAndWait();
            	
		    	LOGGER.warn("maximized Window is not supported");
		    }
		};
		
		// add listener to primaryStage
		primaryStage.widthProperty().addListener(widthListener);
		primaryStage.heightProperty().addListener(heightListener);
		primaryStage.maximizedProperty().addListener(maximizeListener);
	}
	
	public static Main getMain() {
		return main;
	}

	public CloudController getCloudController() {
		return cloudController;
	}

	public AnchorPane getPane() {
		return pane;
	}
}
