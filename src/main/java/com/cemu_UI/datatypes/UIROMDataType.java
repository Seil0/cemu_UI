/**
 * cemu_UI
 * 
 * Copyright 2017-2019  <@Seil0>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

package com.cemu_UI.datatypes;

import java.io.File;

import com.jfoenix.controls.JFXButton;

import javafx.geometry.Insets;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class UIROMDataType extends VBox {
	
	private String romPath;
	private String titleID;
	private String name;
	private Label label = new Label();
	private JFXButton button = new JFXButton();
	private ImageView imageView = new ImageView();
	
	public UIROMDataType(String romPath, String titleID, String name, String coverPath) {
		this();
		
		this.romPath = romPath;
		this.titleID = titleID;
		this.name = name;
		
		label.setText(name);
		imageView.setImage(new Image(new File(coverPath).toURI().toString()));
	}
	
	public UIROMDataType(String romPath, String titleID, String name, String coverPath, ContextMenu contextMenu) {
		this();
		
		this.romPath = romPath;
		this.titleID = titleID;	
		this.name = name;
		
		label.setText(name);
		imageView.setImage(new Image(new File(coverPath).toURI().toString()));
		button.setContextMenu(contextMenu);
	}
	
	public UIROMDataType() {
		super.getChildren().addAll(label, button);
		
		label.setMaxWidth(200);
		label.setPadding(new Insets(0,0,0,8));
		label.setFont(Font.font("System", FontWeight.BOLD, 14));
		
		imageView.setFitHeight(300);
		imageView.setFitWidth(200);
		
		button.setStyle("-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.8), 10, 0, 0, 3); ");
		button.setGraphic(imageView);
	}

	public String getRomPath() {
		return romPath;
	}
	
	public String getTitleID() {
		return titleID;
	}

	public String getName() {
		return name;
	}

	public Label getLabel() {
		return label;
	}

	public JFXButton getButton() {
		return button;
	}

	public ImageView getImageView() {
		return imageView;
	}
	
	public void setRomPath(String romPath) {
		this.romPath = romPath;
	}

	public void setTitleID(String titleID) {
		this.titleID = titleID;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLabel(Label label) {
		this.label = label;
	}

	public void setButton(JFXButton button) {
		this.button = button;
	}

	public void setImageView(ImageView imageView) {
		this.imageView = imageView;
	}
	
}
