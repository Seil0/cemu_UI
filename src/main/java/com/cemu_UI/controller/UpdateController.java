/**
 * cemu_UI
 * 
 * Copyright 2017-2019  <@Seil0>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

package com.cemu_UI.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.swing.ProgressMonitor;
import javax.swing.ProgressMonitorInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cemu_UI.application.MainWindowController;
import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import javafx.application.Platform;

public class UpdateController implements Runnable {

	private MainWindowController mainWindowController;
	private int buildNumber;
	private int updateBuildNumber; // tag_name from gitea
	private String apiOutput;
	@SuppressWarnings("unused")
	private String updateName;
	@SuppressWarnings("unused")
	private String updateChanges;
	private String browserDownloadUrl; // update download link	
	private String giteaApiRelease = "https://git.mosad.xyz/api/v1/repos/Seil0/cemu_UI/releases";
	private URL giteaApiUrl;
	private boolean useBeta;
	private static final Logger LOGGER = LogManager.getLogger(UpdateController.class.getName());

	/**
	 * updater for cemu_UI, checks for Updates and download it
	 */
	public UpdateController(MainWindowController mwc, String buildNumber, boolean useBeta) {
		mainWindowController = mwc;
		this.buildNumber = Integer.parseInt(buildNumber);
		this.useBeta = useBeta;
	}

	@Override
	public void run() {
		LOGGER.info("beta:" + useBeta + "; checking for updates ...");
		Platform.runLater(() -> {
			mainWindowController.getUpdateBtn().setText(XMLController.getLocalBundle().getString("updateBtnChecking"));
		});

		try {
			giteaApiUrl = new URL(giteaApiRelease);
			
			BufferedReader ina = new BufferedReader(new InputStreamReader(giteaApiUrl.openStream()));
			apiOutput = ina.readLine();
			ina.close();
		} catch (IOException e) {
			Platform.runLater(() -> {
				LOGGER.error("could not check update version", e);
			});
		}
		
		JsonArray objectArray = Json.parse("{\"items\": " + apiOutput + "}").asObject().get("items").asArray();
		JsonValue object = objectArray.get(0).asObject(); // set to the latest release as default
		JsonObject objectAsset = object.asObject().get("assets").asArray().get(0).asObject();
		
		for(JsonValue objectIt : objectArray) {
			// TODO note this will download still the pre-release if there's a more recent stable version
			if(objectIt.asObject().getBoolean("prerelease", false) == useBeta) {
				// we found the needed release either beta or not
				object = objectIt;
				objectAsset = objectIt.asObject().get("assets").asArray().get(0).asObject();
				break;
			}
		}
		
		updateBuildNumber = Integer.parseInt(object.asObject().getString("tag_name", ""));
		updateName = object.asObject().getString("name", "");
		updateChanges = object.asObject().getString("body", "");

		LOGGER.info("Build: " + buildNumber + ", Update: " + updateBuildNumber);

		/**
		 * Compare the program BuildNumber with the current BuildNumber
		 * if buildNumber < updateBuildNumber then perform a update
		 */
		if (buildNumber >= updateBuildNumber) {
			Platform.runLater(() -> {
				mainWindowController.getUpdateBtn().setText(XMLController.getLocalBundle().getString("updateBtnNoUpdateAvailable"));
			});
			LOGGER.info("no update available");
		} else {
			Platform.runLater(() -> {
				mainWindowController.getUpdateBtn().setText(XMLController.getLocalBundle().getString("updateBtnUpdateAvailable"));
			});
			LOGGER.info("update available");
			browserDownloadUrl = objectAsset.getString("browser_download_url", "");
			LOGGER.info("download link: " + browserDownloadUrl);
			try {
				// open new Http connection, ProgressMonitorInputStream for downloading the data
				HttpURLConnection connection = (HttpURLConnection) new URL(browserDownloadUrl).openConnection();
				ProgressMonitorInputStream pmis = new ProgressMonitorInputStream(null, "Downloading...", connection.getInputStream());
				ProgressMonitor pm = pmis.getProgressMonitor();
				pm.setMillisToDecideToPopup(0);
				pm.setMillisToPopup(0);
				pm.setMinimum(0);// set beginning of the progress bar to 0
				pm.setMaximum(connection.getContentLength());// set the end to the file length
				FileUtils.copyInputStreamToFile(pmis, new File("cemu_UI_update.jar")); // download update
				org.apache.commons.io.FileUtils.copyFile(new File("cemu_UI_update.jar"), new File("cemu_UI.jar"));
				org.apache.commons.io.FileUtils.deleteQuietly(new File("cemu_UI_update.jar")); // delete update
				new ProcessBuilder("java", "-jar", "cemu_UI.jar").start(); // start the new application
				System.exit(0); // close the current application
			} catch (IOException e) {
				Platform.runLater(() -> {
					LOGGER.info("could not download update files", e);
				});
			}
		}
	}
}
