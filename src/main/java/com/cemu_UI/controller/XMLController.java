/**
 * cemu_UI
 * 
 * Copyright 2017-2019  <@Seil0>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

package com.cemu_UI.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cemu_UI.datatypes.GlobalDataTypes.CloudService;

public class XMLController {

	private static final String userHome = System.getProperty("user.home");
	private static final String userName = System.getProperty("user.name");
	private static final String osName = System.getProperty("os.name");
	private static final String osArch = System.getProperty("os.arch");
	private static final String osVers = System.getProperty("os.version");
	private static final String javaVers = System.getProperty("java.version");
	private static final String javaVend = System.getProperty("java.vendor");
	private static final String sysLocal = System.getProperty("user.language") + "_" + System.getProperty("user.country");
	private static String dirCemuUIPath;
	private static File dirCemuUI;
	private static File configFile;
	private static File gamesDBFile;
	private static File reference_gamesFile;
	private static File pictureCache;
	
	// user settings
	private static String color = "00a8cc";
	private static String usrLocal = sysLocal;
	private static boolean autoUpdate = false;
	private static boolean useBeta = false;
	private static boolean fullscreen = false;
	private static boolean cloudSync = false;
	private static ResourceBundle localBundle = ResourceBundle.getBundle("locals.cemu_UI-Local", Locale.US);
	
	private static String cemuPath;
	private static String romDirectoryPath;
	private static String folderID;
	private static CloudService cloudService;
	private static long lastLocalSync;
	private static double windowWidth;
	private static double windowHeight;
	
	
	private static final Logger LOGGER = LogManager.getLogger(XMLController.class.getName());
	private Properties props = new Properties();

	public XMLController() {
		
		if (osName.contains("Windows")) {
			dirCemuUIPath = userHome + "/Documents/cemu_UI";
		} else {
			dirCemuUIPath = userHome + "/cemu_UI";
		}
		
		// set the concrete files
		dirCemuUI = new File(dirCemuUIPath);
		configFile = new File(dirCemuUI + "/config.xml");
		gamesDBFile = new File(dirCemuUI + "/games.db");
		reference_gamesFile = new File(dirCemuUI + "/reference_games.db");
		pictureCache= new File(dirCemuUI+"/picture_cache");
	}
	
	/**
     * save settings to the config.xml file
     */
	public void saveSettings() {
		LOGGER.info("saving Settings ...");

		try {
			props.setProperty("cemuPath", getCemuPath());
			props.setProperty("romPath", getRomDirectoryPath());
			props.setProperty("color", getColor());
			props.setProperty("language", getUsrLocal());
			props.setProperty("fullscreen", String.valueOf(isFullscreen()));
			props.setProperty("cloudSync", String.valueOf(isCloudSync()));
			props.setProperty("autoUpdate", String.valueOf(isAutoUpdate()));
			props.setProperty("useBeta", String.valueOf(isUseBeta()));
			if (getCloudService() == null) {
				props.setProperty("cloudService", "");
				props.setProperty("folderID", "");
				props.setProperty("lastLocalSync", "");
			} else {
				props.setProperty("cloudService", getCloudService().toString());
				props.setProperty("folderID", getFolderID());
				props.setProperty("lastLocalSync", Long.toString(getLastLocalSync()));
			}

			props.setProperty("windowWidth", Double.toString(getWindowWidth()));
			props.setProperty("windowHeight", Double.toString(getWindowHeight()));

			OutputStream outputStream = new FileOutputStream(XMLController.getConfigFile()); // new output-stream
			props.storeToXML(outputStream, "cemu_UI settings"); // write new .xml
			outputStream.close();
			LOGGER.info("saving Settings done!");
		} catch (IOException e) {
			LOGGER.error("an error occured", e);
		}
	}
	
	/**
     * loading saved settings from the config.xml file
     * if a value is not present, default is used instead
     */
	public void loadSettings() {
		LOGGER.info("loading settings ...");
		try {
			InputStream inputStream = new FileInputStream(XMLController.getConfigFile());
			props.loadFromXML(inputStream); // new input-stream from .xml

			try {
				setCemuPath(props.getProperty("cemuPath"));
			} catch (Exception e) {
				LOGGER.error("cloud not load cemuPath", e);
				setCemuPath("");
			}

			try {
				setRomDirectoryPath(props.getProperty("romPath"));
			} catch (Exception e) {
				LOGGER.error("could not load romPath", e);
				setRomDirectoryPath("");
			}

			try {
				setColor(props.getProperty("color"));
			} catch (Exception e) {
				LOGGER.error("could not load color value, setting default instead", e);
				setColor("00a8cc");
			}

			if (props.getProperty("language") == null) {
				LOGGER.error("cloud not load language, setting default instead");
				setUsrLocal("en_US");
			} else {
				setUsrLocal(props.getProperty("language"));
			}

			try {
				setFullscreen(Boolean.parseBoolean(props.getProperty("fullscreen")));
			} catch (Exception e) {
				LOGGER.error("could not load fullscreen, setting default instead", e);
				setFullscreen(false);
			}

			try {
				setCloudSync(Boolean.parseBoolean(props.getProperty("cloudSync")));
			} catch (Exception e) {
				LOGGER.error("could not load cloudSync, setting default instead", e);
				setCloudSync(false);
			}

			try {
				setAutoUpdate(Boolean.parseBoolean(props.getProperty("autoUpdate")));
			} catch (Exception e) {
				LOGGER.error("cloud not load autoUpdate", e);
				setAutoUpdate(false);
			}

			try {
				setUseBeta(Boolean.parseBoolean(props.getProperty("useBeta")));
			} catch (Exception e) {
				LOGGER.error("cloud not load autoUpdate", e);
				setUseBeta(false);
			}

			try {
				switch (props.getProperty("cloudService")) {
				case "GoogleDrive":
					setCloudService(CloudService.GoogleDrive);
					break;
				case "Dropbox":
					setCloudService(CloudService.Dropbox);
					break;
				default:
					break;
				}
			} catch (Exception e) {
				LOGGER.error("could not load cloudSync", e);
				setCloudService(null);
			}

			try {
				setFolderID(props.getProperty("folderID"));
			} catch (Exception e) {
				LOGGER.error("could not load folderID, disable cloud sync. Please contact an developer", e);
				setCloudSync(false);
			}

			try {
				setLastLocalSync(Long.parseLong(props.getProperty("lastLocalSync")));
			} catch (Exception e) {
				LOGGER.error("could not load lastSuccessSync, setting default instead", e);
				setLastLocalSync(0);
			}

			try {
				setWindowWidth(Double.parseDouble(props.getProperty("windowWidth")));
			} catch (Exception e) {
				LOGGER.error("could not load windowWidth, setting default instead", e);
				setWindowWidth(904);
			}

			try {
				setWindowHeight(Double.parseDouble(props.getProperty("windowHeight")));
			} catch (Exception e) {
				LOGGER.error("could not load windowHeight, setting default instead", e);
				setWindowHeight(600);
			}

			inputStream.close();
			LOGGER.info("loading settings done!");
		} catch (IOException e) {
			LOGGER.error("an error occured", e);
		}
	}
	
	// getters for application variables

	public static String getUserHome() {
		return userHome;
	}

	public static String getUserName() {
		return userName;
	}

	public static String getOsName() {
		return osName;
	}

	public static String getOsArch() {
		return osArch;
	}

	public static String getOsVers() {
		return osVers;
	}

	public static String getJavaVers() {
		return javaVers;
	}

	public static String getJavaVend() {
		return javaVend;
	}

	public static String getSysLocal() {
		return sysLocal;
	}

	public static String getDirCemuUIPath() {
		return dirCemuUIPath;
	}

	public static File getDirCemuUI() {
		return dirCemuUI;
	}

	public static File getConfigFile() {
		return configFile;
	}

	public static File getGamesDBFile() {
		return gamesDBFile;
	}

	public static File getRference_gamesFile() {
		return reference_gamesFile;
	}

	public static File getPictureCache() {
		return pictureCache;
	}

	// getters & setters for user settings

	public static String getColor() {
		return color;
	}

	public static void setColor(String color) {
		XMLController.color = color;
	}

	public static String getUsrLocal() {
		return usrLocal;
	}

	public static void setUsrLocal(String usrLocal) {
		XMLController.usrLocal = usrLocal;
	}

	public static boolean isAutoUpdate() {
		return autoUpdate;
	}

	public static void setAutoUpdate(boolean autoUpdate) {
		XMLController.autoUpdate = autoUpdate;
	}

	public static boolean isUseBeta() {
		return useBeta;
	}

	public static void setUseBeta(boolean useBeta) {
		XMLController.useBeta = useBeta;
	}

	public static boolean isFullscreen() {
		return fullscreen;
	}

	public static void setFullscreen(boolean fullscreen) {
		XMLController.fullscreen = fullscreen;
	}

	public static boolean isCloudSync() {
		return cloudSync;
	}

	public static void setCloudSync(boolean cloudSync) {
		XMLController.cloudSync = cloudSync;
	}
	
	public static ResourceBundle getLocalBundle() {
		return localBundle;
	}

	public static void setLocalBundle(ResourceBundle localBundle) {
		XMLController.localBundle = localBundle;
	}

	public static String getCemuPath() {
		return cemuPath;
	}

	public static void setCemuPath(String cemuPath) {
		XMLController.cemuPath = cemuPath;
	}

	public static String getRomDirectoryPath() {
		return romDirectoryPath;
	}

	public static void setRomDirectoryPath(String romDirectoryPath) {
		XMLController.romDirectoryPath = romDirectoryPath;
	}
	
	
	public static String getFolderID() {
		return folderID;
	}

	public static void setFolderID(String folderID) {
		XMLController.folderID = folderID;
	}

	public static CloudService getCloudService() {
		return cloudService;
	}

	public static void setCloudService(CloudService cloudService) {
		XMLController.cloudService = cloudService;
	}

	public static long getLastLocalSync() {
		return lastLocalSync;
	}

	public static void setLastLocalSync(long lastLocalSync) {
		XMLController.lastLocalSync = lastLocalSync;
	}

	public static double getWindowWidth() {
		return windowWidth;
	}

	public static void setWindowWidth(double windowWidth) {
		XMLController.windowWidth = windowWidth;
	}

	public static double getWindowHeight() {
		return windowHeight;
	}

	public static void setWindowHeight(double windowHeight) {
		XMLController.windowHeight = windowHeight;
	}
}
