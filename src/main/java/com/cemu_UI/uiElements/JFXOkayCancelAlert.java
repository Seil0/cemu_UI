/**
 * cemu_UI
 * 
 * Copyright 2017-2019  <@Seil0>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

package com.cemu_UI.uiElements;

import com.cemu_UI.controller.XMLController;
import com.jfoenix.controls.JFXAlert;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialogLayout;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class JFXOkayCancelAlert {
	
	private String headingText;
	private String bodyText;
	private String btnStyle;
	private Stage stage;
	
	private EventHandler<ActionEvent> okayAction;
	private EventHandler<ActionEvent> cancelAction;
	
	private String okayText = XMLController.getLocalBundle().getString("okayBtnText");
	private String cancelText = XMLController.getLocalBundle().getString("cancelBtnText");
	
	/**
	 * 
	 * @param headingText	Heading text of the alert
	 * @param bodyText		Content text of the alert
	 * @param btnStyle		Style of the buttons
	 * @param stage			Stage to which the Alert belongs
	 */
	public JFXOkayCancelAlert(String headingText, String bodyText, String btnStyle, Stage stage) {
		setHeadingText(headingText);
		setBodyText(bodyText);
		setBtnStyle(btnStyle);
		setStage(stage);
	}
	
	public void showAndWait( ) {
		JFXAlert<Void> alert = new JFXAlert<>(stage);
		
    	JFXButton okayBtn = new JFXButton(okayText);
    	okayBtn.addEventHandler(ActionEvent.ACTION, (e)-> alert.close());
    	okayBtn.addEventHandler(ActionEvent.ACTION, okayAction);
    	okayBtn.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
    	okayBtn.setPrefHeight(32);
    	okayBtn.setStyle(btnStyle);
    	
    	JFXButton cancelBtn = new JFXButton(cancelText);
    	cancelBtn.addEventHandler(ActionEvent.ACTION, (e)-> alert.close());
    	cancelBtn.addEventHandler(ActionEvent.ACTION, cancelAction);
    	cancelBtn.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
    	cancelBtn.setPrefHeight(32);
    	cancelBtn.setStyle(btnStyle);
    	
		
		JFXDialogLayout content = new JFXDialogLayout();
		content.setActions(cancelBtn, okayBtn);
		content.setHeading(new Text(headingText));
		content.setBody(new Text(bodyText));
		alert.setContent(content);
		alert.showAndWait();
	}

	public String getHeadingText() {
		return headingText;
	}

	public void setHeadingText(String headingText) {
		this.headingText = headingText;
	}

	public String getBodyText() {
		return bodyText;
	}

	public void setBodyText(String bodyText) {
		this.bodyText = bodyText;
	}
	
	public String getBtnStyle() {
		return btnStyle;
	}

	public void setBtnStyle(String btnStyle) {
		this.btnStyle = btnStyle;
	}
	
	public String getOkayText() {
		return okayText;
	}

	public void setOkayText(String okayText) {
		this.okayText = okayText;
	}
	
	public String getCancelText() {
		return cancelText;
	}

	public void setCancelText(String cancelText) {
		this.cancelText = cancelText;
	}
	
	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public EventHandler<ActionEvent> getOkayAction() {
		return okayAction;
	}

	public void setOkayAction(EventHandler<ActionEvent> okayAction) {
		this.okayAction = okayAction;
	}

	public EventHandler<ActionEvent> getCancelAction() {
		return cancelAction;
	}

	public void setCancelAction(EventHandler<ActionEvent> cancelAction) {
		this.cancelAction = cancelAction;
	}

}
