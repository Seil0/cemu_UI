/**
 * cemu_UI
 * 
 * Copyright 2017-2019  <@Seil0>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

package com.cemu_UI.uiElements;

import com.jfoenix.controls.JFXAlert;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextArea;

import javafx.scene.text.Text;
import javafx.stage.Stage;

public class JFXTextAreaAlert {
	
	private String headingText;
	private String textAreaText;
	private String btnStyle;
	private Stage stage;
	
	/**
	 * Creates a new JFoenix Alert to show some information on a TextArea
	 * @param headingText	Heading text of the alert
	 * @param textAreaText	Content text of the alert
	 * @param btnStyle		Style of the okay button
	 * @param stage			Stage to which the Alert belongs
	 */
	public JFXTextAreaAlert(String headingText, String textAreaText, String btnStyle, Stage stage) {
		setHeadingText(headingText);
		setTextAreaText(textAreaText);
		setBtnStyle(btnStyle);
		setStage(stage);
	}
	
	public void showAndWait( ) {
		JFXAlert<Void> alert = new JFXAlert<>(stage);
		JFXTextArea textArea = new JFXTextArea(textAreaText);
		textArea.setMinWidth(450);
		
		JFXButton button = new JFXButton("Okay");
		button.setOnAction(event -> alert.close());
		button.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
		button.setPrefHeight(32);
		button.setStyle(btnStyle);
		
		JFXDialogLayout content = new JFXDialogLayout();
		content.setActions(button);
		content.setHeading(new Text(headingText));
		content.setBody(textArea);
		alert.setContent(content);
		alert.showAndWait();
	}

	public String getHeadingText() {
		return headingText;
	}

	public String getTextAreaText() {
		return textAreaText;
	}

	public String getBtnStyle() {
		return btnStyle;
	}

	public Stage getStage() {
		return stage;
	}

	public void setHeadingText(String headingText) {
		this.headingText = headingText;
	}

	public void setTextAreaText(String textAreaText) {
		this.textAreaText = textAreaText;
	}

	public void setBtnStyle(String btnStyle) {
		this.btnStyle = btnStyle;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

}
