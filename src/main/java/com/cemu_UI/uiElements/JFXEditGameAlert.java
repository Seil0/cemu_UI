/**
 * cemu_UI
 * 
 * Copyright 2017-2019  <@Seil0>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

package com.cemu_UI.uiElements;

import java.io.File;

import com.cemu_UI.application.MainWindowController;
import com.cemu_UI.controller.XMLController;
import com.jfoenix.controls.JFXAlert;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialogLayout;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class JFXEditGameAlert {
	
	private String headingText;
	private String bodyText;
	private String title = "";
	private String coverPath = "";
	private String romPath = "";
	private String titleID = "";
	private String btnStyle;
	private int mode;
	private Stage stage;
	private MainWindowController mwc; // TODO remove this!
	
	/**
	 * Creates a new JFoenix Alert to show some information with okay and cancel option
	 * @param headingText	Heading text of the alert
	 * @param bodyText		Content text of the alert
	 * @param btnStyle		Style of the buttons
	 * @param mode			Set to 0 for add and 1 for edit mode
	 * @param stage			Stage to which the Alert belongs
	 * @param mwc			a mwc dep, it's horrible i know
	 */
	public JFXEditGameAlert(String headingText, String bodyText, String btnStyle, int mode, Stage stage, MainWindowController mwc) {
		this.headingText = headingText;
		this.bodyText = bodyText;
		this.btnStyle = btnStyle;
		this.mode = mode;
		this.stage = stage;
		this.mwc = mwc;
	}
	
	public void showAndWait( ) {
		JFXAlert<Void> alert = new JFXAlert<>(stage);
		
    	TextField gameTitleTF = new TextField();
    	gameTitleTF.setPromptText(XMLController.getLocalBundle().getString("gameTitle"));
    	TextField gameTitleIDTF = new TextField();
    	gameTitleIDTF.setPromptText(XMLController.getLocalBundle().getString("titleID"));
    	TextField romPathTF = new TextField();
    	romPathTF.setPromptText(XMLController.getLocalBundle().getString("romPath"));
    	TextField gameCoverTF = new TextField();
    	gameCoverTF.setPromptText(XMLController.getLocalBundle().getString("coverPath"));
    	
    	if (mode == 1) {
			gameTitleTF.setText(title);
			gameTitleIDTF.setText(titleID);
			romPathTF.setText(romPath);
			gameCoverTF.setText(coverPath);
			
			gameTitleIDTF.setEditable(false);
		}
    	
    	JFXButton okayBtn = new JFXButton(XMLController.getLocalBundle().getString("okayBtnText"));
		okayBtn.setOnAction(event -> {
			if (romPathTF.getText().toString().length() == 0 || gameCoverTF.getText().toString().length() == 0
					|| gameTitleTF.getText().toString().length() == 0
					|| gameTitleIDTF.getText().toString().length() == 0) {

				// addGame error dialog
				JFXInfoAlert errorDialog = new JFXInfoAlert(
						XMLController.getLocalBundle().getString("editGameDialogHeadingTextError"),
						XMLController.getLocalBundle().getString("editGameDialogBodyTextError"), btnStyle, stage);
				errorDialog.showAndWait();
			} else {
				switch (mode) {
				case 0:
					// add-mode title, coverPath, romPath, titleID
					mwc.addBtnReturn(gameTitleTF.getText().toString(), gameCoverTF.getText().toString(),
							romPathTF.getText().toString(), gameTitleIDTF.getText().toString());
					break;
				case 1:
					// edit mode
					mwc.editBtnReturn(gameTitleTF.getText().toString(), gameCoverTF.getText().toString(),
							romPathTF.getText().toString(), gameTitleIDTF.getText().toString());
					break;
				default:
					break;
				}

				alert.close();
			}
		});
    	okayBtn.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
    	okayBtn.setPrefHeight(32);
    	okayBtn.setStyle(btnStyle);
    	
    	JFXButton cancelBtn = new JFXButton(XMLController.getLocalBundle().getString("cancelBtnText"));
    	cancelBtn.addEventHandler(ActionEvent.ACTION, (e)-> alert.close());
    	cancelBtn.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
    	cancelBtn.setPrefHeight(32);
    	cancelBtn.setStyle(btnStyle);
    	
		JFXButton selectPathBtn = new JFXButton(XMLController.getLocalBundle().getString("editGameDialogSelectPathBtn"));
		selectPathBtn.setPrefWidth(110);
		selectPathBtn.setStyle(btnStyle);
		selectPathBtn.setOnAction(event -> {
			FileChooser romDirectoryChooser = new FileChooser();
			File romDirectory = romDirectoryChooser.showOpenDialog(stage);
			romPathTF.setText(romDirectory.getAbsolutePath());
		});
    	
    	JFXButton selectCoverBtn = new JFXButton(XMLController.getLocalBundle().getString("editGameDialogSelectCoverBtn"));
    	selectCoverBtn.setPrefWidth(110);
    	selectCoverBtn.setStyle(btnStyle);
		selectCoverBtn.setOnAction(event -> {
			FileChooser coverDirectoryChooser = new FileChooser();
			File coverDirectory = coverDirectoryChooser.showOpenDialog(stage);
			gameCoverTF.setText(coverDirectory.getAbsolutePath());
		});
		
		GridPane grid = new GridPane();
    	grid.setHgap(10);
    	grid.setVgap(10);
    	grid.setPadding(new Insets(15, 10, 10, 10));
    	grid.add(new Label(XMLController.getLocalBundle().getString("gameTitle") + ":"), 0, 0);
    	grid.add(gameTitleTF, 1, 0);
    	grid.add(new Label(XMLController.getLocalBundle().getString("titleID") + ":"), 0, 1);
    	grid.add(gameTitleIDTF, 1, 1);
    	grid.add(new Label(XMLController.getLocalBundle().getString("romPath") + ":"), 0, 2);
    	grid.add(romPathTF, 1, 2);
    	grid.add(selectPathBtn, 2, 2);
    	grid.add(new Label(XMLController.getLocalBundle().getString("coverPath") + ":"), 0, 3);
    	grid.add(gameCoverTF, 1, 3);
    	grid.add(selectCoverBtn, 2, 3);
    	
    	VBox vbox = new VBox();
    	vbox.getChildren().addAll(new Text(bodyText), grid);
		
		JFXDialogLayout content = new JFXDialogLayout();
		content.setActions(cancelBtn, okayBtn);
		content.setHeading(new Text(headingText));
		content.setBody(vbox);
		alert.setContent(content);
		alert.showAndWait();
	}

	public String getHeadingText() {
		return headingText;
	}

	public String getBodyText() {
		return bodyText;
	}

	public String getTitle() {
		return title;
	}

	public String getCoverPath() {
		return coverPath;
	}

	public String getRomPath() {
		return romPath;
	}

	public String getTitleID() {
		return titleID;
	}

	public String getBtnStyle() {
		return btnStyle;
	}

	public int getMode() {
		return mode;
	}

	public Stage getStage() {
		return stage;
	}

	public void setHeadingText(String headingText) {
		this.headingText = headingText;
	}

	public void setBodyText(String bodyText) {
		this.bodyText = bodyText;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setCoverPath(String coverPath) {
		this.coverPath = coverPath;
	}

	public void setRomPath(String romPath) {
		this.romPath = romPath;
	}

	public void setTitleID(String titleID) {
		this.titleID = titleID;
	}

	public void setBtnStyle(String btnStyle) {
		this.btnStyle = btnStyle;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

}
