# cemu_UI

![Total Downloads](https://img.shields.io/github/downloads/Seil0/cemu_UI/total.svg?style=flat-square)
[![Latest](https://img.shields.io/badge/release-085-blue.svg?style=flat-square)](https://git.mosad.xyz/Seil0/cemu_UI/releases)
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0)

cemu_UI is a simple, material design graphical frontend for [cemu](http://cemu.info/), a Wii U emulator. Downloads can be found [here](https://git.mosad.xyz/Seil0/cemu_UI/releases).

## Installation
Simply download the cemu_UI.jar from [Releases](https://git.mosad.xyz/Seil0/cemu_UI/releases), make sure you have the latest version of java 8 oracle jre/jdk installed and open the file. cemu_UI creats a new directory "C:\Users\USERNAME\Documents\cemu_UI", where the database, settings and covers are stored. **first start can take while!**

If you want to use the cloud sync function read the [wiki](https://git.mosad.xyz/Seil0/cemu_UI/wiki#cloud-savegame-syncronisation) carefully!

### [FAQ](https://git.mosad.xyz/Seil0/cemu_UI/wiki#faq)

## Features

* launch Games
* Time played in total
* last time played
* add updates and dlcs easier (only adding not downloading!)
* automatic rom detection (only .rpx files with a app.xml)
* customisable UI
* [sync savegames via google drive](https://git.mosad.xyz/Seil0/cemu_UI/wiki)
* [smmdb api](https://github.com/Tarnadas/smmdb) integration

## [planed Features](https://git.mosad.xyz/Seil0/cemu_UI/milestones)

* Controller support

### If you have another idea, make a new issue!

### [building from source](https://git.mosad.xyz/Seil0/cemu_UI/wiki/Documentation)
  
## Screenshots

![Screenshot](https://www.mosad.xyz/images/cemu_UI_MainWindow.webp)
